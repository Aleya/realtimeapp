<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
<div id="app">
    <v-app>
{{--        <v-toolbar>--}}
{{--            <v-toolbar-title>Title</v-toolbar-title>--}}

{{--            <v-toolbar-items>--}}
{{--                <v-btn text>Link 1</v-btn>--}}
{{--                <v-btn text>Link 2</v-btn>--}}
{{--                <v-btn text>Link 3</v-btn>--}}
{{--            </v-toolbar-items>--}}

{{--        </v-toolbar>--}}
        <example-component></example-component>
    </v-app>
</div>
<script src="{{asset('js/app.js')}}"></script>

</body>
</html>